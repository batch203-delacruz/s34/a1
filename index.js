
const express = require("express");

const port = 4000;

const app = express();

// middleware

app.use(express.json());

// mock data
let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];

	let items = [
		{
			name: "Mjolnir",
			price: 50000,
			isActive: true
		},
		{
			name: "Vibranium Shield",
			price: 70000,
			isActive: true
		}
];


app.get('/', (req, res) => {
	res.send('Hello from my first expressJS API');
});

app.get('/greeting', (req, res) => {
	res.send('Hello from Batch203-delacruz');
});

app.get("/users", (req, res) => {
	res.send(users);
});

// Adding of new user

app.post('/users', (req, res) => {

	console.log(req.body);

	// simulate creating an new user doc
	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	};

	// adding newUser to the existing array
	users.push(newUser);
	console.log(users);

	// send the updated users array in the client
	res.send(users);
});

// DELETE Method
app.delete('/users', (req, res) => {

	users.pop();
	res.send(users);
});

// PUT Method
// update user's password
// :index - wildcard
// url:localhost:4000/users/0

app.put('/users/:index', (req, res) => {

	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index);

	users[index].password = req.body.password;
	res.send(users[index]);
});


/*
	Activity : 30 mins

	    Endpoint: /items

	    >> Create a new route to get and send items array in the client (GET ALL ITEMS)
*/
app.get('/items', (req, res) => {
	res.send(items);
});

/*
	    >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
	        >> send the updated items array in the client
	        >> check the post method route for our users for reference
*/
app.post('/items', (req, res) => {
	console.log(req.body);

	let newItem = {
			name: req.body.name,
			price: req.body.price,
			isActive: req.body.isActive
		}
	
		items.push(newItem);
		console.log(items);

		res.send(items);

	});
/*
	    >> Create a new route which can update the price of a single item in the array (UPDATE ITEM)
	        >> pass the index number of the item that you want to update in the request params
	        >> add the price update in the request body
	        >> reassign the new price from our request body
	        >> send the updated item in the client

*/

app.put('/items/:index', (req, res) => {
	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index); 
	items[index].price = req.body.price;
	res.send(items[index]);
});

/*
	    >> Create a new collection in Postman called s34-activity
	    >> Save the Postman collection in your s34 folder
*/




app.listen(port, () => console.log(`Server is running at port ${port}`));

